//
//  SchoolsTableViewController.swift
//  20210820-JeffreyFlores-NYCSchools
//
//  Created by Jeffrey Flores on 8/21/21.
//

import UIKit

//Global Variable used  to set currently selected school throughout app.
var indexForSelectedSchool: IndexPath!
//Class used to show the schools list in a table view
class SchoolsTableViewController: UITableViewController {
    //School data used to populate the tableview
    let schoolData: [[String]] = SchoolsData.shared.data
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set this porperty to zero to have tableview separator go to edges
        self.tableView.separatorInset = .zero
        self.clearsSelectionOnViewWillAppear = false
        
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // On section needed to display this data list
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Using School Data property to return amount of rows
        return schoolData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath)
        let schoolDataSet: [String] = schoolData[indexPath.row]
        
        // Using School Data to populate School Names on each cell
        cell.textLabel?.text = schoolDataSet[0]
        cell.textLabel?.font = UIFont(name: "Trebuchet MS", size: 12)
        cell.textLabel?.numberOfLines = 0
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Set global variable to selected index Path here to get the data out of the Array using this index throughout the app.
        indexForSelectedSchool = indexPath
        //Show view that will show SAT scores for the selected school
        performSegue(withIdentifier: "showScores", sender: self)
        
        
    }
    
    
    
    
    
    
    
    
}

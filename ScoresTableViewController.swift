//
//  ScoresTableViewController.swift
//  20210820-JeffreyFlores-NYCSchools
//
//  Created by Jeffrey Flores on 8/21/21.
//

import UIKit
//Table View Controller to display the SAT Scores respectively that go with each School
class ScoresTableViewController: UITableViewController {
    
    //Property used to populate the table view on this controller with the current school that was selected in the previous view SAT Scores
    let currentSchool: [String] = SchoolsData.shared.data[indexForSelectedSchool.row]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set inset to zero to have separator to edge of screen
        self.tableView.separatorInset = .zero
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // Only one section for this list of data
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Four rows for the amount of results returned
        return 4
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Switch statement is returning the specific result for each cell based on the IndexPath 
        switch indexPath.row {
        case 0:
            let studentsTakenCell = tableView.dequeueReusableCell(withIdentifier: "studentsTakenCell", for: IndexPath(row: 0, section: 1))
            
            
            studentsTakenCell.textLabel?.text = "Number of Test Takers: \(currentSchool[1])"
            return studentsTakenCell
        case 1:
            let satMathScoreCell = tableView.dequeueReusableCell(withIdentifier: "satMathScoreCell", for: IndexPath(row: 1, section: 1))
            satMathScoreCell.textLabel?.text = "Math Avg. Score: \(currentSchool[3])"
            return satMathScoreCell
        case 2:
            let satReadingScoreCell = tableView.dequeueReusableCell(withIdentifier: "satReadingScoreCell", for: IndexPath(row: 2, section: 1))
            satReadingScoreCell.textLabel?.text = "Critical Reading Avg. Score: \(currentSchool[2])"
            return satReadingScoreCell
        case 3:
            let satWritingScoreCell = tableView.dequeueReusableCell(withIdentifier: "satWritingScoreCell", for: IndexPath(row: 3, section: 1))
            satWritingScoreCell.textLabel?.text = "Writing Avg. Score: \(currentSchool[4])"
            return satWritingScoreCell
        default:
            return UITableViewCell()
        }
        
    }
    
}
